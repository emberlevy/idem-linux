CLI_CONFIG = {}
CONFIG = {
    "file_buffer_size": {
        "dyne": "grains",
        "default": 262144,
        "help": "chunk size for transported files",
        "type": int,
    }
}
SUBS = {}
DYNE = {
    "grains": ["grains"],
    "exec": ["exec"],
    "states": ["states"],
}

import pytest
import mock
from dict_tools import data


@pytest.mark.asyncio
async def test_load_selinux(mock_hub, hub):
    mock_hub.exec.cmd.run.side_effect = [
        data.NamespaceDict({"retcode": 0}),
        data.NamespaceDict({"stdout": "Enabled"}),
    ]
    with mock.patch("shutil.which", return_value=True):
        mock_hub.grains.linux.system.selinux.load_selinux = (
            hub.grains.linux.system.selinux.load_selinux
        )
        await mock_hub.grains.linux.system.selinux.load_selinux()
    assert mock_hub.grains.GRAINS.selinux.enabled is True
    assert mock_hub.grains.GRAINS.selinux.enforced == "Enabled"
